# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon. It is a simple server written in python, modified to serve HTML and CSS files in addition to the previous ASCII cat functionality.

## Instructions:
---------------

- Download the repo and use the included Makefile.

- Go to 0.0.0.0:5000/trivia.html and bask in the glory.

## Authors
---------------

### Original Version:
* **UOCIS322** [proj1-pageserver](https://bitbucket.org/UOCIS322/proj1-pageserver/)

### Updates to Serve HTML and CSS:

* **Cole Sabin** [E-Mail](mailto:csabin@uoregon.edu)

